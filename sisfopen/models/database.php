<?php  
class Database{
    private $host;
    private $user;
    private $pass;
    private $database;
    public $conn;

    function __construct($host, $user, $pass, $database){
        $this->host = $host;
        $this->user = $user;
        $this->pass = $pass;
        $this->database = $database;

        // MEMULAI KONEKSI DATABASE DENGAN DATA DARI CONFIG/KONEKSI.PHP
        $this->conn = new mysqli($this->host = $host, $this->user = $user,$this->pass = $pass, $this->database = $database) or die (mysqli_error());
        if(!$this->conn){
            return false;
        } else {
            return true;
        }
    }

}
?>