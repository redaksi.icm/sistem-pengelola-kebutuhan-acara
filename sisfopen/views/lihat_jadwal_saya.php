<?php
    include "models/m_pegawai.php";
    include "models/m_penugasan.php";
    $pgw = new Pegawai($connection);
    $tgs = new Penugasan($connection);
?>
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <!-- column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-block">
                <h4 class="card-title">Daftar Tugas Saya</h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>Nama Acara</th>
                                <th>Produser Acara</th>
                                <th>Sebagai</th>
                                <th>Lokasi</th>
                                <th>Tanggal Mulai</th>
                                <th>Tanggal Selesai</th>
                                <th> </th>
                        
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        $NIP = $_SESSION['login_user']->NIP;
                        $sql = "SELECT * FROM tbl_penugasan p, tbl_jadwal j WHERE p.NIP = '".$NIP."' AND p.KODE_JADWAL = j.KODE_JADWAL AND j.STATUS != 'DELETED' ORDER BY j.CREATED_AT DESC";
                        $tampil = $tgs->query($sql);
                        $sql = "SELECT * FROM tbl_jadwal j, tbl_pegawai p WHERE j.PRODUSER_NIP = '".$NIP."' AND j.PRODUSER_NIP = p.NIP AND j.STATUS != 'DELETED'";
                        $tampil_produser = $tgs->query($sql);
                        if (!$tampil && !$tampil_produser) {
                        ?>
                            <tr>
                                <td colspan="7">Tidak Dapat Menampilkan Data</td>
                            </tr>
                        <?php
                        } else {
                            while($data = $tampil_produser->fetch_object()){
                            ?>
                            <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $data->NAMA_ACARA; ?></td>
                                <td><?php echo $data->NAMA.' ('.$data->PRODUSER_NIP.')'; ?></td>
                                <td><?php echo $_SESSION['login_user']->JABATAN; ?></td>
                                <td><?php echo $data->LOKASI; ?></td>
                                <td><?php echo $data->TANGGAL_MULAI; ?></td>
                                <td><?php echo $data->TANGGAL_SELESAI; ?></td>
                                <td>
                                    <a href="?page=lihat_jadwal_detail<?php echo '&detail='.$data->KODE_JADWAL;?>"> <button type="button" class="btn waves-effect waves-light btn-info btn-xs">DETAIL</button></a>
                                    <a href="./../sisfoinventori?page=peminjaman_input"> <button type="button" class="btn waves-effect waves-light btn-warning btn-xs">PINJAM BARANG</button></a>
                                </td>
                            </tr>
                            <?php
                            }
                            while($data = $tampil->fetch_object()){
                                $get_produser = $pgw->tampil_filter('NIP', $data->PRODUSER_NIP)->fetch_object();
                            ?>
                            <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $data->NAMA_ACARA; ?></td>
                                <td><?php echo $get_produser->NAMA.' ('.$data->PRODUSER_NIP.')'; ?></td>
                                <td><?php echo $_SESSION['login_user']->JABATAN; ?></td>
                                <td><?php echo $data->LOKASI; ?></td>
                                <td><?php echo $data->TANGGAL_MULAI; ?></td>
                                <td><?php echo $data->TANGGAL_SELESAI; ?></td>
                                <td>
                                    <a href="?page=lihat_jadwal_detail<?php echo '&detail='.$data->KODE_JADWAL;?>"> <button type="button" class="btn waves-effect waves-light btn-info btn-xs">DETAIL</button></a>
                                </td>
                            </tr>
                            <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->