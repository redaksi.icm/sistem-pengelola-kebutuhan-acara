<?php
    include "models/m_pegawai.php";
    $pgw = new Pegawai($connection);
?>

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <!-- column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-block">
                <?php
                    if (isset($_POST['ubah'])) {
                        if (isset($_POST['ubah'])) {
                            $data = $pgw->ubah($_POST);
                            $message = 'Diubah';
                        }
                        if ($data) {
                            $login = $pgw->tampil($_POST['NIP']);
                            $login = $login->fetch_object();
                            $_SESSION['login_user'] = $login;
                            echo '
                            <div class="alert alert-success"> <i class="ti-user"></i> Data Berhasil '.$message.'.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            </div>
                            ';
                        } else {
                            echo '
                            <div class="alert alert-danger"> <i class="ti-user"></i> Data Gagal '.$message.'.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            </div>
                            ';
                        }
                    }
                    $NIP = '';
                    $NAMA = '';
                    $JABATAN = '';
                    $EMAIL = '';
                    $PASSWORD = '';
                    if (isset($_SESSION['login_user']->NIP)) {
                        $data = $pgw->tampil_filter('NIP', $_SESSION['login_user']->NIP);
                        $data = $data->fetch_object();
                        $NIP = $data->NIP;
                        $NAMA = $data->NAMA;
                        $JABATAN = $data->JABATAN;
                        $EMAIL = $data->EMAIL;
                        $PASSWORD = $data->PASSWORD;
                    }
                ?>
                <form action="" class="form-material" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="nip" class="col-md-12">NIP <small>(10 digit angka)</small></label>
                                <div class="col-md-12">
                                    <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" name="NIP" value="<?php echo $NIP;?>" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="nama" class="col-md-12">Nama</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="NAMA" id="nama" value="<?php echo $NAMA;?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-4">Jabatan</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="JABATAN" value="<?php echo $JABATAN;?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email" class="col-md-6">Email</label>
                                <div class="col-md-12">
                                    <input type="email" class="form-control" name="EMAIL" value="<?php echo $EMAIL;?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-12">Password</label>
                                <div class="col-md-12">
                                    <input type="password" class="form-control" name="PASSWORD" value="<?php echo $PASSWORD;?>" required>
                                </div>
                            </div>
                        </div>                    
                    </div>                    
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input type="submit" class="btn btn-success" name="ubah" value="UBAH & SIMPAN"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->