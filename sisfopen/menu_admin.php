<!-- HALAMAN UNTUK MEMBUAT MENU DI ADMIN -->
<ul id="sidebarnav">
    
    <li> <a class="waves-effect waves-dark" href="?page=buat_jadwal" aria-expanded="false"><i class="mdi mdi-table-edit"></i><span class="hide-menu">Buat Jadwal</span></a>
    </li>
    <li> <a class="waves-effect waves-dark" href="?page=lihat_jadwal" aria-expanded="false"><i class="mdi mdi-table"></i><span class="hide-menu">Lihat Jadwal</span></a>
    </li>
    <li> <a class="waves-effect waves-dark" href="?page=manage_pegawai" aria-expanded="false"><i class="mdi mdi-account-check"></i><span class="hide-menu">Manage Pegawai</span></a>
    </li>
    <li> <a class="waves-effect waves-dark" href="auth/logout.php" aria-expanded="false"><i class="mdi mdi-power"></i><span class="hide-menu">SIGN OUT</span></a>
    </li>
</ul>
