<?php
    date_default_timezone_set("Asia/Jakarta");

    echo date('d-m-Y H:i:s');
    echo "<br>";

    ini_set('max_execution_time', 0);
    set_time_limit(0);

    require_once('./../phpmailer/mail.php');
    require_once('./../config/koneksi.php');
    require_once('./../config/database.php');

    $mail = new Mail();
    $mysqli = new Database($host, $user, $pass, $database);
    $db = $mysqli->conn;

    $sql = "SELECT * FROM tbl_notifikasi WHERE email_status = 'pending' OR email_status = 'gagal'";
    $query = $db->query($sql) or ($db->error);
    if ($query->num_rows !== 0) {
        while($data = $query->fetch_object()) {
            $recipients = Array(
                'NIP' => NULL,
                'NAMA' => NULL,
                'JABATAN' => NULL,
                'EMAIL' => NULL,
                'TELEPON' => NULL,
                'PASSWORD' => NULL,
                'CREATED_AT' => NULL
            );
            $sql = "SELECT * FROM tbl_pegawai WHERE NIP = '$data->penerima'";
            $queryUser = $db->query($sql) or ($db->error);
            if ($queryUser->num_rows !== 0) {
                $recipients = $queryUser->fetch_object();
            }
            $message = $data->pesan_html;
            $title = 'Aplikasi Sistem Penjadwalan Kerabat Kerja dan Peralatan Berbasis IT';
            $subject = 'Aplikasi Sistem Penjadwalan Kerabat Kerja dan Peralatan Berbasis IT';
            $resp = $mail->sendMessage($recipients, $title, $subject, $message);
            if ($resp->status === 1 || $resp->status === true) {
                $status = "sukses";
            } else {
                if ($data->email_status === 'gagal') {
                    $status = "batal";
                } else {
                    $status = "gagal";
                }
            }
            $detail_status = json_encode($resp);
            $sql = "UPDATE tbl_notifikasi SET email_status = '$status', email_detail = '$detail_status' WHERE id = $data->id";
            $update = $db->query($sql) or ($db->error);
            echo "<pre>";
            print_r($resp);
        }
    }
    echo "<br>";
?>